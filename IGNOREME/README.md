**EDITING THIS FOLDER IS USELESS, EDIT THE ONE CALLED arena_lib IN THE WORLD FOLDER INSTEAD**  
  
The first time the world is launched with `_world_folder_media` on, this folder is copy-pasted into the world folder (with the sole exception of this file). The purpose of this folder is to put any media that you want loaded in-game into this folder once it has been copied to the world folder.